**Installing and configuring standalone iSCSI Target**
# Setup environment
* Hostname: SAN
* IP address: 192.168.43.11/24
* OS: RHEL 7.7 x86_64
* Disk 1 (sda) 30GB used for OS
* Disk 2 (sdb) 60GB used for iSCSI Target LUN
* LVM configuration:
  * Physical volume: `/dev/sdb`
  * Volume Group: `vg_iscsi`
  * Logical Volume: `lv_iscsi_01` for testing
# Keywords
* `iqn.year-month.reverse_domain_name`: -> iSCSI qualified name
* Can use `year-month.reverse_domain_name` or what you want
* `target0` -> Identify the using target, 1st target in this server
* `/dev/vg_iscsi/lv_iscsi_00` -> define LUN0 location from logical volume created
* `initiator-address` -> define the iSCSI client allow to connect
* `iscsi-user` -> define the authentication username and password connected to iSCSI Target Server

# Pre-requisites
* Verify *Disk2 (/dev/sdb)* available
```
fdisk -l
```
* The result should be similar as below
```
# fdisk -l

Disk /dev/sdb: 64.4 GB, 64424509440 bytes, 125829120 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes

```

* Create Physical Volume from `/dev/sdb`
```
# pvcreate <physical device/disk>
pvcreate /dev/sdb &\
pvs
```

* Create Volume Group - named `vg_iscsi`
```
# vgcreate <volume group name> <physical volume>
vgcreate vg_iscsi /dev/sdb &\
vgs
```

* Create Logical Volume - named `lv_iscsi_00` 20GB for iSCSI Target LUN0
```
# lvcreate -L <volume size> -n <volume name> <volume group>
lvcreate -L 20GB -n lv_iscsi_00 vg_iscsi &\
lvs
```

# Implement iSCSI Target Server
* Install iSCSI Target package
```
yum install -y targetcli
```
* Enable and start iSCSI target service
```
systemctl enable target
systemctl start target
```

* Start configuring iSCSI target with command `targetcli`
```
targetcli
```

* Navigate to `/backstore/block` and start create our back store with our logical volume `lv_iscsi_00`
```
cd /backstores/block
create lv_iscsi_00 /dev/vg_target/lv_iscsi_00
```

* Navigate to `/iscsi` and start create an iSCSI Target
```
cd /iscsi
create iqn.2020-02.local.lab:target0
```

* Create LUN and map it to our back store created above step
```
ls
cd iqn.2020-02.local.lab:target0/tpg1/luns
create /backstores/block/lv_iscsi_00
```

* Navigate to ACL under the created tarkget, then create an ACL to allow iSCSI iniator (client) machine to access, in case windows iscsi initiator name: `iqn.2020-02.local.lab:win-initiator` and linux iscsi initiator name: `iqn.2020-02.local.lab:lin-initiator`
```
cd ../acls
create iqn.2020-02.local.lab:win-initiator
create iqn.2020-02.local.lab:lin-initiator
```

* Save configuration
```
cd /
saveconfig
```

* Verify iSCSI target configuration and exit
```
ls
exit
```

* Restart iSCSI Target service to apply the change
```
systemctl restart target
```

* Local firewall settings
```
firewall-cmd --permanent --new-ipset=NET_SCSI --type=hash:net
firewall-cmd --permanent --ipset=NET_SCSI --add-entry=192.168.43.1
firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source ipset=NET_SCSI service name=iscsi-target
firewall-cmd --reload
```

# How to connect iSCSI Target LUN
## Connect iSCSI Target server from Windows
* Click on key *WIN* or press *Start* 
* In search, type *iSCSI Initiator* to start iSCSI initiator client
* Navigate to *Configuration* tab, then change initiator name to `iqn.2020-02.local.lab:win-initiator`
* Navigate to *Discovery* tab, then click on *Discover Portal* to add iSCSI target server
* Type iSCSI target IP or hostname and keep same default port if you not change
* Navigate back to *Targets* tab, then you will see iSCSI target list
* Select target, then cick on *Connect*
* Finally you can check new disk connected from Windows Disk managment console, with command: `diskmgmt.msc`

## Connect iSCSI Target server from Linux (RHEL, CentOS)
* Install iSCSI initiator package
```
yum install -y iscsi-initiator-utils
```

* Set the initiator name
```
sed -i 's/InitiatorName=*.*/InitiatorName=iqn.2020-02.local.lab:lin-initiator/' /etc/iscsi/initiatorname.iscsi
```

* Enable and start iSCSI initiator service
```
systemctl enable iscsi --now
```

* Execute the `iscsiadm` command in discovery mode with the server ip address `192.168.43.11`
```
iscsiadm --mode discovery --type sendtargets --portal 192.168.43.11
```

* Execute the iscsiadm command in node mode with the server ip address `192.168.43.11`
```
iscsiadm --mode node --targetname iqn.2020-02.local.lab:target0 --portal 192.168.43.11 --login
```

* To verify new target disk connected, use this command: `lsblk --scsi`